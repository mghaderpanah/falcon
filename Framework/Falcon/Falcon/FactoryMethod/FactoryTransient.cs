﻿
namespace Falcon.FactoryMethod
{
    #region sealed class FactoryTransient<T>
    /// <summary>
    /// concrete class for transient Factory Mettod pattern.
    /// </summary>
    /// <typeparam name="T">product type</typeparam>
    public sealed class FactoryTransient<T> : FactoryAbstract<T> where T : new() 
    {
        /// <summary>
        /// creates transient instance of product {T}.
        /// </summary>
        /// <returns>transient instance of the product</returns>
        public override T CreateProduct()
        {
            return new T();
        }
    }
    #endregion
}
