﻿using System;

namespace FalconTest.Mock
{
    public class Cigar : IProduct, IDisposable
    {
        public string Name { get; set; }
        public string Price { get; set; }
        
        public ITobacco Filler { get; set; }
        public ITobacco Binder { get; set; }
        public ITobacco Wrapper { get; set; }
        
        private bool _disposed;

        public override string ToString()
        {
            return "Cigar:" + Name + ";" + "Price:" + Price + ";" +
                    (Filler != null ? Filler.Make() : "") +
                    (Binder != null ? Binder.Make() : "") +
                    (Wrapper != null ? Wrapper.Make() : "");
        }

        /// <summary>
        /// Implement IDisposable.
        /// Do not make this method virtual.
        /// A derived class should not be able to override this method.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose(bool disposing) executes in two distinct scenarios.
        /// If disposing equals true, the method has been called directly
        /// or indirectly by a user's code. Managed and unmanaged resources
        /// can be disposed.
        /// If disposing equals false, the method has been called by the
        /// runtime from inside the finalizer and you should not reference
        /// other objects. Only unmanaged resources can be disposed.
        /// </summary>
        /// <param name="disposing"></param>
        public virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (_disposed) return;

            // If disposing equals true, dispose all managed and unmanaged resources.
            if (disposing)
            {
                // TODO: dispose managed state (managed objects).
            }

            // Call the appropriate methods to clean up unmanaged resources here.
            // If disposing is false, only the following code is executed.
            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.


            // TODO : base class if applicable
            //base.Dispose(pDisposing);

            // Note disposing has been done.
            _disposed = true;
        }
    }
}
