﻿
namespace FalconTest.Mock
{
    public class Tobacco : ITobacco
    {
        protected string Brand { get; set; }
        protected string Country { get; set; }

        protected bool _disposed;

        public string Make()
        {
            return Brand + "," + Country + ";";
        }
    }
}
