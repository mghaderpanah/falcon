﻿using System;

namespace FalconTest.Mock
{
    public class WrapperRomeoJulieta : Tobacco, IDisposable
    {
        public WrapperRomeoJulieta()
        {
            Brand = "Wrapper:Romeo y Julieta";
            Country = "Cuba";
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                // TODO: dispose managed state (managed objects).
            }

            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.

            // TODO : base class if applicable
            //base.Dispose(pDisposing);

            _disposed = true;
        }
    }
}
